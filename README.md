# Writing applications with Angular2, [part3] #

In my previous posts I was checking how to [build and bind together multiple Angular 2 components](https://bitbucket.org/tomask79/angular2-components/overview), I explained 
to you how [Dependency Injection](https://bitbucket.org/tomask79/angular2-services-di/overview) works in Angular 2 and how to Inject an Angular 2 service into component. 
Since Angular 2 is super highly modular and component based, Angular team needed to figure out effective system of **component interactions**. Let's take a look on them!

## Angular 2 and main ways of component interactions ##

* Parent interacts with child via a local variable...(**We will test this in this demo!**)
* Parent calls a ViewChild...(**We will test this in the next demo..;**)
* Parent listens for child event...(**Using of EventEmmiter or Observables, I will give it a try later as well**)
* Parent and children communicate via a service...(**I already showed you this in previous demo**...)

## Parent interacts with child via a local variable ##

Alright, let's create a **simple selectable product list component**, which is going to be our root component. And let's have a basket component nested inside
for showing how much you're going to pay for buyed products.

### Principle ###

First, let's show our **nested child component** which will be exposing it's API via local variable:

```
import {Component} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'basket',
    template: `
        <div>
            <img src='http://histmag.org/static/1393709076_shopping-cart.png' />
            $To pay: {{toPayAmount}}</div>
            <button (click)='emptyBasket()' style='float: right'>CLEAR BASKET</button>
    `
})
export class BasketComponent {
    private toPayAmount: number = 0;

    public buy(product: ProductType): void {
        this.toPayAmount = this.toPayAmount + product.price;
    }

    public emptyBasket(): void {
        this.toPayAmount = 0;
    }
}
```
Okay, what we want to achieve is to expose **buy method** to our root component, so our basket will be updated with selected
product. And how we're going to do that? To define a local variable in a template for accessing the component API you need
to add attribute to component selector tag **#<name of local variable>**.  Our root component accessing the child basket component
via local variable will look like:

```
import {Component, OnInit} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {BasketComponent} from './basket.component';

@Component({
    selector: 'my-app',
    template: `<h1>Simple product buying demo with components iteractions</h1>
            <div style='float: right; border: 3px solid'>
                <basket #basket></basket>
            </div>
            <ul>
                <li *ngFor="#product of _products">
                    <div>{{product.name}} for {{product.price}}$ <button (click)="basket.buy(product)">BUY</button></div>
                    <img src={{product.image}} />
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [BasketComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}
```
* Notice how we defined **local #basket variable in the basket tag**.
* Also notice how we passed selected product to buy method of Basket component via mentioned variable "**(click)="basket.buy(product)"**"
* OnInit TS interface and ngOnInit method is just a component [Life Cycle hook](https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html) to perform products load on component initialization.

## Testing the demo ##

* git clone <this repo>
* npm install (in the angular2-components-interaction directory)
* npm start
* visit http://localhost:3000

Now try to click at some "buy" button and you should see basket updated by the product prize. 

Hope you found this usefull

regards

Tomas