export class ProductType {
    constructor(public name:string, public price:number, public image: string) {
    }
}